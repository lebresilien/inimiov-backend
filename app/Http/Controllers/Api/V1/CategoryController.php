<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('products')->get();
        return response()->json($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:categories'],
            'description' => ['nullable', 'string'],
        ]);

        Category::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'description' => $request->description,
        ]);

        return response()->noContent();
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->with('products')->first();

        if(!$category)  return response()->json([
            "message" =>  "Aucune catogorie trouvée.",
            "errors" => [
                "message" => "Aucune catogorie trouvée"
            ]
        ], 422);

        return response()->json($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:categories,slug,'.$slug],
            'description' => ['nullable', 'string'],
        ]);

        $category = Category::where('slug', $slug)->first();

        if(!$category)  return response()->json([
            "message" =>  "Aucune categorie trouvée.",
            "errors" => [
                "message" => "Aucune categorie trouvée"
            ]
        ], 422);

        $category->update([
            "name" => $request->name,
            //"slug" => Str::slug($request->name, '-'),
            "description" => $request->description
        ]);

        return response()->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $category = Category::where('slug', $slug)->first();

        if(!$category)  return response()->json([
            "message" =>  "Aucune catogorie trouvée.",
            "errors" => [
                "message" => "Aucune catogorie trouvée"
            ]
        ], 422);

        $category->delete();
    }
}
