<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ Category, Product };

class DashboardController extends Controller
{
    public function __invoke() {
        
        $count_category = Category::all();
        $count_product = Product::all();
        $count_product_not_available = Product::where('available', 0)->get();

        return response()->json([
            "product" => $count_product->count(),
            "category" => $count_category->count(),
            "product_not_available" => $count_product_not_available->count(),
        ]);
    }
}
