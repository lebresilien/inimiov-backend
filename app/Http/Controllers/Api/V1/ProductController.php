<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Str;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('category')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'category_id' => ['required', 'exists:categories,id'],
            'price' =>  ['regex:/^[0-9]+(\.[0-9][0-9]?)?$/'],
            'available' =>  ['regex:/^[0-9]+(\.[0-9][0-9]?)?$/'],
            'image' =>  ['required', 'string'],
            'description' => ['required', 'string'],
            'image' => ['required']
        ]);

        
        $image = $request->get('image');
        $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
        \Image::make($request->get('image'))->save(storage_path('app/public/images/').$name);
        
        $product = Product::create([
            "name" => $request->name,
            "slug" => Str::slug($request->name, '-'),
            "category_id" => $request->category_id,
            "price" => $request->price,
            "available" => $request->available,
            "description" => $request->description,
            "image" => $name,
        ]);

        return response()->noContent();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search($seacrh_text) {

        $products = Product::where("name", "like", "%".$seacrh_text."%")->get();

        return response()->json($products);
    }
}
