<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create(['name' => 'admin']);
        $user = Role::create(['name' => 'user']);

        Permission::create(['name' => 'create category']);
        Permission::create(['name' => 'list category']);
        Permission::create(['name' => 'show category']);
        Permission::create(['name' => 'delete category']);
        Permission::create(['name' => 'create product']);
        Permission::create(['name' => 'show product']);

        $adminRole->givePermissionTo('create category');
        $adminRole->givePermissionTo('list category');
        $adminRole->givePermissionTo('show category');
        $adminRole->givePermissionTo('delete category');


    }
}
