<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\{ User };
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {

            $user = User::create([
                'name' => 'John Doe',
                'email' => 'john.doe@gmail.com',
                'password' => Hash::make('12345678'),
                'email_verified_at' => now()
            ]);

            // assign role to user
            $user->assignRole('admin');

            // send validation mail to user
            //event(new Registered($user));

            Auth::login($user);

            DB::commit();

        } catch(\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }
}
