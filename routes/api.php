<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\CategoryController;
use App\Http\Controllers\Api\V1\DashboardController;
use App\Http\Controllers\Api\V1\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum' ],  function($route) {

    $route->get('categories', [CategoryController::class, 'index']);
    $route->post('categories', [CategoryController::class, 'store']);
    $route->get('categories/{slug}', [CategoryController::class, 'show']);
    $route->put('categories/{slug}', [CategoryController::class, 'update']);
    $route->delete('categories/{slug}', [CategoryController::class, 'destroy']);

    $route->get('dashboard', DashboardController::class);

    $route->get('products', [ProductController::class, 'index']);
    $route->post('products', [ProductController::class, 'store']);
   
});

Route::get('v1/categories-welcome', [CategoryController::class, 'index']);
Route::get('v1/products/search/{search_text}', [ProductController::class, 'search']);

 
